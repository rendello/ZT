#!/usr/bin/python3.6

import random
import discord
from discord.ext import commands

from secret_token import token


async def zalgo_ify(text):
    ''' Takes some normal text and zalgo-ifies it '''

    # Chars in "Combining Diacritical Marks" Unicode block.
    combining_chars = [chr(n) for n in range(768, 878)]

    zalgo_text = ''

    for char in text:
        combining_char = random.choice(combining_chars)
        zalgo_text += f'{char}{combining_char}'

    return zalgo_text


bot = commands.Bot(command_prefix="!")


# The following commands is called automatically
# on messaging a connected server with !zt,
# !zalgo, or !z
@bot.command(aliases=['zalgo', 'z'])
async def zt(ctx):
    message = str(ctx.message.content)

    zalgo_text = await zalgo_ify(message)
    await ctx.send(zalgo_text)


bot.run(token)
