
![ZALGO HEADER](Artwork/zalgo_header.jpg "ZALGO HEADER")
This very simple Discord bot was created for the publication *[Paged
Out!](https://pagedout.institute/)* as an illustration that writing bots can be
very easy, even for novice programmers!

At 17 lines of actual code, the bot is dead-simple, and transforms a user’s
comment into a spooky-looking "Zalgo" version.

![ZALGO ARTICLE](Artwork/article.jpg "ZALGO ARTICLE")
